import {Dish} from '../shared/dish';
import { DishService } from '../services/dish.service';
import { DishdetailComponent } from '../dishdetail/dishdetail.component';
import { NgModule } from '@angular/core';

import { Component, OnInit, Inject } from '@angular/core';


  /*@NgModule({
    declarations: [
      
      DishdetailComponent
    ],
    
  })*/

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {


  dishes: Dish[] ;

  constructor(private dishService: DishService,
    @Inject('baseURL') private baseURL) { }
  
  ngOnInit() {
    this.dishService.getDishes().subscribe(dishes => this.dishes = dishes);

  }
  
  

}

